FROM maven:3-jdk-8-slim AS build
WORKDIR /app
COPY . .
RUN mvn -B package

FROM tomcat:10.0.18-jre8-openjdk-slim
COPY --from=build /app/target/app.war "$CATALINA_HOME/webapps/ROOT.war"
COPY apache-tomcat-10.0.18/conf/server.xml "$CATALINA_HOME/conf"
COPY apache-tomcat-10.0.18/conf/certs "$CATALINA_HOME/conf/certs"
EXPOSE 8080
EXPOSE 8443
