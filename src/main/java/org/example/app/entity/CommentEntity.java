package org.example.app.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CommentEntity {
    private String id;
    private String ticket_id;
    private String author_id;
    private String author_name;
    private String text;
}
