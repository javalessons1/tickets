package org.example.app.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TicketEntity {
    private String id;
    private String author_id;
    private String author_name;
    private String title;
    private String content;
    private String status;
}
