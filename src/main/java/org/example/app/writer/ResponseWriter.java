package org.example.app.writer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import jakarta.servlet.http.HttpServletResponse;
import org.example.app.dto.CommentDto;
import org.example.app.dto.TicketDto;
import org.example.app.entity.CommentEntity;
import org.example.app.entity.TicketEntity;
import org.example.app.service.CommentService;
import org.example.app.service.TicketService;
import org.example.framework.authentication.LoginPasswordAuthentication;
import org.example.framework.security.Authentication;
import org.example.framework.security.Roles;

import java.io.IOException;
import java.util.List;

public class ResponseWriter {
    public static void writeResponse(HttpServletResponse response, String ticketId, CommentService commentService, TicketService ticketService, LoginPasswordAuthentication authentication) throws IOException {
        List<CommentEntity> comments = commentService.seeTicketComments(ticketId, authentication);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        response.setContentType("application/json");
        TicketEntity ticket = ticketService.getTicket(ticketId);
        if (authentication.getRole().equals(Roles.ROLE_SUPPORT) || authentication.getId().equals(ticket.getAuthor_id())) {
            JsonElement jsonTicket = gson.toJsonTree(new TicketDto(
                            ticket.getAuthor_name(),
                            ticket.getTitle(),
                            ticket.getContent(),
                            ticket.getStatus()),
                    TicketDto.class);
            response.getWriter().write(gson.toJson(jsonTicket));
            for (CommentEntity comment : comments) {
                JsonElement jsonComment = gson.toJsonTree(new CommentDto(
                                comment.getAuthor_name(),
                                comment.getText()),
                        CommentDto.class);
                response.getWriter().write( gson.toJson(jsonComment));
            }

        }
        else{
            response.getWriter().write(gson.toJson("Not enough privileges!"));
        }
    }
}
