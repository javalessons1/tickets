package org.example.app.service;

import lombok.RequiredArgsConstructor;
import org.example.app.entity.TicketEntity;
import org.example.app.exception.DataAccessException;
import org.example.app.exception.TicketNotFoundException;
import org.example.app.repository.TicketRepository;
import org.example.framework.authentication.LoginPasswordAuthentication;
import org.example.framework.security.Roles;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@RequiredArgsConstructor
@Service
public class TicketService {

    private final TicketRepository ticketRepository;

    public TicketEntity create(LoginPasswordAuthentication authentication, TicketEntity ticket) throws IOException {
        if (authentication.getRole().equals(Roles.ROLE_SUPPORT)) {
            throw new DataAccessException("Not enough privileges!");
        }
        return ticketRepository.insertTicket(authentication.getId(), authentication.getLogin(), ticket);
    }

    public TicketEntity getTicket(String ticketId) {
        return ticketRepository.getTicket(ticketId);
    }

    public void closeTicket(String ticketId, LoginPasswordAuthentication authentication, String status) {
        try {
            TicketEntity ticket = ticketRepository.getTicket(ticketId);
            if (authentication.getId().equals(ticket.getAuthor_id())) {
                if(status!=null){
                    if (status.equalsIgnoreCase("CLOSED") || status.equalsIgnoreCase("OPEN") ) {
                        ticketRepository.closeTicket(ticketId, status);
                    }
                    else{
                        throw new DataAccessException("Can't change status!");
                    }
                }
            }
            else {
                throw new DataAccessException("Access denied!");
            }
        } catch (Exception e) {
            throw new TicketNotFoundException("No such ticket!");
        }
    }

    public List<TicketEntity> getAll(LoginPasswordAuthentication authentication) throws IOException {
        List<TicketEntity> tickets;
        if (authentication.getRole().equals(Roles.ROLE_USER)) {
            tickets = ticketRepository.getAllAuthorTickets(authentication.getId());
            return tickets;
        }
        if(authentication.getRole().equals(Roles.ROLE_SUPPORT)){
            tickets = ticketRepository.getAllTickets();
            return tickets;
        }
        return null;
    }

}
