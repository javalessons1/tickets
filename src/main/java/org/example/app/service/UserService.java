package org.example.app.service;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.example.app.repository.UserRepository;
import org.example.framework.authentication.Authentication;
import org.example.framework.authentication.LoginPasswordAuthentication;
import org.example.framework.authenticator.Authenticator;
import org.example.framework.exception.AuthenticationException;
import org.example.framework.exception.UserNotFoundException;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserService implements Authenticator {
  private final UserRepository repository;

  @Override
  public Authentication authenticate(String login, String password) throws AuthenticationException {
    return repository.getByLogin(login)
        .map(o -> new LoginPasswordAuthentication(o.getLogin(), o.getPassword(), o.getRole(), o.getId()))
        .orElseThrow(UserNotFoundException::new);
  }

  public LoginPasswordAuthentication basicAuthenticate(HttpServletRequest request){
    final Authentication authentication = (Authentication) request.getAttribute(Authentication.ATTR_AUTH);
    if (authentication.isAnonymous() == true) {
      LoginPasswordAuthentication anonimous = new LoginPasswordAuthentication("", "", "anonimous", "-1");

      return anonimous;
    }
    return (LoginPasswordAuthentication) authentication;
  }
}
