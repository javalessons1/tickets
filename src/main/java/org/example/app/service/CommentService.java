package org.example.app.service;

import com.google.gson.Gson;
import jakarta.servlet.http.HttpServletRequest;

import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.example.app.entity.CommentEntity;
import org.example.app.exception.TicketNotFoundException;
import org.example.app.repository.CommentRepository;
import org.example.app.repository.TicketRepository;
import org.example.framework.authentication.LoginPasswordAuthentication;
import org.example.framework.security.Authentication;
import org.example.framework.security.Roles;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@RequiredArgsConstructor
@Service
public class CommentService {
    private final CommentRepository commentRepository;
    private final TicketRepository ticketRepository;

    public void createComment(LoginPasswordAuthentication authentication, String ticketId, CommentEntity comment) throws IOException {
        try {
            if (ticketRepository.getTicket(ticketId)!=null) {
                if (authentication.getRole().equals(Roles.ROLE_SUPPORT)) {
                    commentRepository.insertComments(ticketId, authentication.getId(), authentication.getLogin(), comment);
                }
            }
        } catch (Exception e) {
            throw new TicketNotFoundException("No such ticket!");
        }
    }

    public List<CommentEntity> seeTicketComments(String ticketId, LoginPasswordAuthentication authentication) {
        try {
            if (authentication.getRole().equals(Roles.ROLE_SUPPORT)||authentication.getRole().equals(Roles.ROLE_USER)) {
                List<CommentEntity> ticket = commentRepository.getComments(ticketId);
                return ticket;
            }
            throw new TicketNotFoundException("Not enough privileges!");
        }
        catch (Exception e){
            throw new TicketNotFoundException("No such ticket");
        }
    }
}

