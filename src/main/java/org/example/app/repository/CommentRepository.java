package org.example.app.repository;
import lombok.RequiredArgsConstructor;
import org.example.app.entity.CommentEntity;
import org.jdbi.v3.core.Jdbi;
import org.springframework.stereotype.Repository;

import java.util.*;

@RequiredArgsConstructor
@Repository
public class CommentRepository {
    private final Jdbi jdbi;
    public CommentEntity insertComments(String ticketId, String authorId, String authorLogin, CommentEntity newComment) {
        Optional<CommentEntity> ticketEntity = null;
        try {
            ticketEntity = jdbi.withHandle(handle -> handle.createQuery(
                "INSERT INTO comments (id, ticket_id, author_id, author_name, text) VALUES (:id, :ticketId, :authorId, :authorName, :text) returning *")
                .bind("id", UUID.randomUUID())
                .bind("ticketId", ticketId)
                .bind("authorId", authorId)
                .bind("authorName", authorLogin)
                .bind("text", newComment.getText())
                .mapToBean(CommentEntity.class).findFirst());
    } catch (Exception e) {
        e.printStackTrace();
    }
        return ticketEntity.get();
    }

    public List<CommentEntity> getComments(String ticketId) {
        List<CommentEntity> comments = jdbi.withHandle(handle -> handle.createQuery(
                        "SELECT * from comments WHERE ticket_id = :ticketId")
                .bind("ticketId", ticketId)
                .mapToBean(CommentEntity.class)
                .list());
        return comments;
    }
}