package org.example.app.repository;
import lombok.RequiredArgsConstructor;
import org.example.app.entity.TicketEntity;
import org.jdbi.v3.core.Jdbi;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Repository
public class TicketRepository {
    private final Jdbi jdbi;

    public TicketEntity getTicket(String ticketId) {
        final TicketEntity ticket = jdbi.withHandle(handle -> handle.createQuery(
                        "SELECT * from tickets WHERE id = :ticketId")
                .bind("ticketId", ticketId)
                .mapToBean(TicketEntity.class)
                .findFirst()).get();
        return ticket;
    }
    public TicketEntity insertTicket(String authorId, String authorLogin, TicketEntity ticket) {
        Optional<TicketEntity> ticketEntity = null;
        try {
            ticketEntity = jdbi.
                    withHandle(handle -> handle.createQuery(
                                    //languge=PostgreSQL
                            "insert into tickets (id, author_id, author_name, title, content) " +
                                    "values (:Id, :authorId, :authorName, :title, :content) returning *"
                            )
                            .bind("Id", UUID.randomUUID())
                            .bind("authorId", authorId)
                            .bind("authorName", authorLogin)
                            .bind("title", ticket.getTitle())
                            .bind("content", ticket.getContent())
                            .mapToBean(TicketEntity.class).findFirst());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ticketEntity.get();
    }
    public TicketEntity closeTicket(String ticketId, String newStatus) {
        Optional<TicketEntity> ticketEntity = null;
        try {
            ticketEntity = jdbi.withHandle(handle -> handle.createQuery(
                        "UPDATE tickets SET status = :status WHERE id = :ticketId returning *")
                .bind("ticketId", ticketId)
                .bind("status", newStatus)
                .mapToBean(TicketEntity.class)
                .findFirst());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ticketEntity.get();
    }

    public List<TicketEntity> getAllAuthorTickets(String authorId){
        final List<TicketEntity> tickets =  jdbi.withHandle(handle -> handle.createQuery(
                        "SELECT * from tickets WHERE author_id = :authorId")
                .bind("authorId", authorId)
                .mapToBean(TicketEntity.class)
                .list());
        return tickets;
    }
    public List<TicketEntity> getAllTickets(){
        final List<TicketEntity> tickets =  jdbi.withHandle(handle -> handle.createQuery(
                        "SELECT * from tickets")
                .mapToBean(TicketEntity.class)
                .list());
        return tickets;
    }
}
