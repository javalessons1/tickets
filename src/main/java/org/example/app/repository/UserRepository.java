package org.example.app.repository;

import lombok.RequiredArgsConstructor;
import org.example.app.entity.UserEntity;
import org.jdbi.v3.core.Jdbi;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@RequiredArgsConstructor
@Repository
public class UserRepository {
  private final Jdbi jdbi;

  public Optional<UserEntity> getByLogin(final String login) {
    return jdbi.withHandle(handle -> handle.createQuery(
            // language=PostgreSQL
            "SELECT id, login, password, role FROM users WHERE login = :login"
        )
        .bind("login", login)
        .mapToBean(UserEntity.class)
        .findFirst());
  }

}
