package org.example.app.controller;

import com.google.gson.Gson;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.app.entity.TicketEntity;
import org.example.app.entity.UserEntity;
import org.example.app.repository.UserRepository;
import org.example.app.service.CommentService;
import org.example.app.service.TicketService;
import org.example.app.service.UserService;
import org.example.app.writer.ResponseWriter;
import org.example.framework.authentication.LoginPasswordAuthentication;
import org.example.framework.exception.AuthenticationException;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import static org.example.framework.routes.Routes.routes;

@Controller
public class TicketController {
    private final CommentService commentService;
    private final TicketService ticketService;
    private final UserService userService;
    private final UserRepository userRepository;

    public TicketController(TicketService service, Gson gson, CommentService commentService, TicketService ticketService, UserService userService, UserRepository userRepository) {
        this.commentService = commentService;
        this.ticketService = ticketService;
        this.userService = userService;
        this.userRepository = userRepository;
        routes.put("/notes/getAll", this::getAll);
        routes.put("/notes/create", this::create);
        routes.put("/notes/closed", this::closeTicket);
    }

    public void create(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final LoginPasswordAuthentication authentication = userService.basicAuthenticate(request);
        TicketEntity ticket = new Gson().fromJson(request.getReader(), TicketEntity.class);
        TicketEntity createdTicket = ticketService.create(authentication, ticket);
        ResponseWriter.writeResponse(response, createdTicket.getId(), commentService, ticketService, authentication);
    }

    public void closeTicket(HttpServletRequest request, HttpServletResponse response) throws IOException {
        LoginPasswordAuthentication authentication = userService.basicAuthenticate(request);
        String ticketId = request.getParameter("id").toString();
        String newStatus = request.getParameter("newStatus");
        ticketService.closeTicket(ticketId, authentication, newStatus.toUpperCase(Locale.ROOT));
        ResponseWriter.writeResponse(response, ticketId, commentService, ticketService, authentication);
    }

    public void getAll(HttpServletRequest request, HttpServletResponse response) throws IOException, AuthenticationException {
        LoginPasswordAuthentication authentication = userService.basicAuthenticate(request);
        List<TicketEntity> tickets = ticketService.getAll(authentication);
        if (tickets == null){
            response.getWriter().write("You must be authorized!");
        }
        else {
            for (TicketEntity ticket : tickets) {
                ResponseWriter.writeResponse(response, ticket.getId(), commentService, ticketService, authentication);
            }
        }

    }
}
