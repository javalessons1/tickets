package org.example.app.controller;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.app.service.UserService;
import org.example.framework.authentication.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.io.IOException;

@Controller
public class UserController {
  private final UserService service;

  public UserController(UserService service) {
    this.service = service;
  }

  public void me(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
    // TODO:
    final Authentication authentication = (Authentication) req.getAttribute(Authentication.ATTR_AUTH);
    resp.getWriter().write("me: " + authentication.getLogin());
  }
}
