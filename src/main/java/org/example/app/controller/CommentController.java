package org.example.app.controller;

import com.google.gson.Gson;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.app.entity.CommentEntity;
import org.example.app.service.CommentService;
import org.example.app.service.TicketService;
import org.example.app.service.UserService;
import org.example.app.writer.ResponseWriter;
import org.example.framework.authentication.LoginPasswordAuthentication;
import org.springframework.stereotype.Controller;

import java.io.IOException;

import static org.example.framework.routes.Routes.routes;

@Controller
public class CommentController {
    private final CommentService commentService;
    private final TicketService ticketService;
    private final UserService userService;

    public CommentController(CommentService service, Gson gson, CommentService commentService, TicketService ticketService, UserService userService) {
        this.commentService = commentService;
        this.ticketService = ticketService;
        this.userService = userService;
        routes.put("/notes/comment/id", this::createComment);
        routes.put("/notes/id", this::seeTicketComment);
    }

    public void createComment(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        String ticketId = request.getParameter("id");
        CommentEntity newComment = new Gson().fromJson(request.getReader(), CommentEntity.class);
        LoginPasswordAuthentication authentication = userService.basicAuthenticate(request);
        commentService.createComment(authentication, ticketId, newComment);
        ResponseWriter.writeResponse(response, ticketId, commentService, ticketService, authentication);
    }

    public void seeTicketComment(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        String ticketId = request.getParameter("id");
        LoginPasswordAuthentication authentication = userService.basicAuthenticate(request);
        ResponseWriter.writeResponse(response, ticketId, commentService, ticketService, authentication);
    }
}
