package org.example.framework.authenticator;

import jakarta.servlet.http.HttpServletRequest;
import org.example.framework.authentication.Authentication;
import org.example.framework.authentication.LoginPasswordAuthentication;
import org.example.framework.exception.AuthenticationException;

public interface Authenticator {
  Authentication authenticate(final String login, final String password) throws AuthenticationException;

  LoginPasswordAuthentication basicAuthenticate(HttpServletRequest request);
}
