package org.example.framework.security;

import lombok.Builder;
import lombok.Data;
import lombok.Setter;

import java.security.Principal;
import java.util.UUID;

@Builder
@Data
public class Authentication implements Principal {
  public static final Authentication ANONYMOUS = Authentication.builder()
      .id(String.valueOf(UUID.randomUUID()))
      .login("anonymous")
      .password("***")
      .role(Roles.ROLE_ANONYMOUS)
      .build();
  private final String id;
  private final String login;
  private final String role;
  @Setter
  private String password;

  @Override
  public String getName() {
    return role;
  }
}
