package org.example.framework.authentication;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class LoginPasswordAuthentication implements Authentication {
  private final String login;
  private String password;
  private String role;
  private String id;


  @Override
  public String getLogin() {
    return login;
  }
  public String getPassword() {
    return password;
  }
  public String getRole() {
    return role;
  }
  public String getId() {
    return id;
  }

  @Override
  public boolean isAnonymous() {
    return false;
  }

  @Override
  public void eraseCredentials() {
    password = null;
  }
}
