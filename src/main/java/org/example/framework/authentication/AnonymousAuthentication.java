package org.example.framework.authentication;

public class AnonymousAuthentication implements Authentication {
  public static final String ANONYMOUS = "anonymous";

  @Override
  public String getLogin() {
    return ANONYMOUS;
  }
  @Override
  public boolean isAnonymous() {
    return true;
  }

  @Override
  public void eraseCredentials() {
  }
}
