package org.example.framework.listener;

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import org.example.app.exception.ContextInitializeException;
import org.example.framework.attribute.ContextAttributes;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ContextLoadDestroyListener implements ServletContextListener {
  private ConfigurableApplicationContext springContext;

  @Override
  public void contextInitialized(final ServletContextEvent sce) {
    try {
      final AnnotationConfigApplicationContext springContext = new AnnotationConfigApplicationContext("org.example.app");
      this.springContext = springContext;
      sce.getServletContext().setAttribute(ContextAttributes.ATTR_SPRING_CONTEXT, springContext);

    } catch (Exception e) {
      throw new ContextInitializeException(e);
    }
  }

  @Override
  public void contextDestroyed(ServletContextEvent sce) {
    if (springContext != null) {
      springContext.close();
    }
    ServletContextListener.super.contextDestroyed(sce);
  }
}
